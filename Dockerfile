FROM maven
RUN mkdir -p /usr/local/myjavaWORKDIR /usr/local/myjava
COPY target/*.jar bin.jar
CMD ["java", "-jar", "bin.jar"]
